import math
import tensorflow as tf

from utilities import weight_bias

class Dropout:
    def __init__(self, keep_prob, name='dropout'):
        self.keep_prob = keep_prob
        self.name = name

    def apply(self, x, model):
        with tf.name_scope(self.name):
            keep_prob = tf.select(model.is_training, self.keep_prob, 1.0)
            return tf.nn.dropout(x, keep_prob)

class Dense:
    def __init__(self, fan_out, name='dense'):
        self.fan_out = fan_out
        self.name = name

    def apply(self, x, model):
        with tf.name_scope(self.name):
            input_shape = x.get_shape()
            fan_in = input_shape[-1].value
            stddev = math.sqrt(1.0 / fan_in)

            shape = [fan_in, self.fan_out]
            W, b = weight_bias(shape, stddev=stddev, bias_init=0.0)

            tf.histogram_summary(W.name, W)
            tf.histogram_summary(b.name, b)

            return tf.matmul(x, W) + b

class Activation:
    def __init__(self, activation, name='activation'):
        self.name = name
        self.activation = activation

    def apply(self, x, model):
        with tf.name_scope(self.name):
            h = self.activation(x)
            tf.histogram_summary(h.name, h)
            return h

class MaxPool:
    def __init__(self, ksize, strides, padding='VALID', name='max_pool'):
        self.ksize = ksize
        self.strides = strides
        self.padding = padding
        self.name = name

    def apply(self, x, model):
        with tf.name_scope(self.name):
            return tf.nn.max_pool(x, self.ksize, self.strides, self.padding)

class AvgPool:
    def __init__(self, ksize, strides, padding='VALID', name='avg_pool'):
        self.ksize = ksize
        self.strides = strides
        self.padding = padding
        self.name = name

    def apply(self, x, model):
        with tf.name_scope(self.name):
            return tf.nn.avg_pool(x, self.ksize, self.strides, self.padding)

class Conv2D:
    def __init__(self, filter_shape, output_channels, strides, padding='VALID', name='conv2d'):
        self.filter_shape = filter_shape
        self.output_channels = output_channels
        self.strides = strides
        self.padding = padding
        self.name = name

    def apply(self, x, model):
        with tf.name_scope(self.name):
            input_shape = x.get_shape()
            input_channels = input_shape[-1].value

            k_w, k_h = self.filter_shape
            stddev = math.sqrt(2.0 / ((k_w * k_h) * input_channels))

            shape = self.filter_shape + [input_channels, self.output_channels]
            W, b = weight_bias(shape, stddev=stddev, bias_init=0.0)

            tf.histogram_summary(W.name, W)
            tf.histogram_summary(b.name, b)

            return tf.nn.conv2d(x, W, self.strides, self.padding) + b

class Flatten:
    def __init__(self, name='flatten'):
        self.name = name

    def apply(self, x, model):
        with tf.name_scope(self.name):
            shape = x.get_shape()
            dim = shape[1] * shape[2] * shape[3]
            return tf.reshape(x, [-1, dim.value])
