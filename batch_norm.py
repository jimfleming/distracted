import tensorflow as tf

from tensorflow.python import control_flow_ops

class Conv2DBatchNorm:
    """
    Batch normalization on convolutional maps.
    Args:
        x:           Tensor, 4D BHWD input maps
        fan_out:       integer, depth of input maps
        scope:       string, variable scope
        affine:      whether to affine-transform outputs
    Return:
        normed:      batch-normalized maps
    """
    def __init__(self, fan_out, affine=True, name='batch_norm'):
        self.fan_out = fan_out
        self.affine = affine
        self.name = name

    def apply(self, x, model):
        with tf.name_scope(self.name):
            beta = tf.Variable(tf.constant(0.0, shape=[self.fan_out]), name='beta', trainable=True)
            gamma = tf.Variable(tf.constant(1.0, shape=[self.fan_out]), name='gamma', trainable=self.affine)

            batch_mean, batch_var = tf.nn.moments(x, [0, 1, 2], name='moments')

            ema = tf.train.ExponentialMovingAverage(decay=0.9)
            ema_apply_op = ema.apply([batch_mean, batch_var])
            ema_mean, ema_var = ema.average(batch_mean), ema.average(batch_var)

            def mean_var_with_update():
                with tf.control_dependencies([ema_apply_op]):
                    return tf.identity(batch_mean), tf.identity(batch_var)

            mean, var = control_flow_ops.cond(model.is_training, mean_var_with_update, lambda: (ema_mean, ema_var))

            return tf.nn.batch_norm_with_global_normalization(x, mean, var, beta, gamma, 1e-3, self.affine)
